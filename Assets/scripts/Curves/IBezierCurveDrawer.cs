﻿using System.Collections.Generic;

namespace Assets
{
    public interface IBezierCurveDrawer<T> where T : BezierCurve
    {
        void Draw(T bezierCurve);
        void Draw(IEnumerable<T> bezierCurves);
    }
}