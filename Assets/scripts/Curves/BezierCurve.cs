﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public abstract class BezierCurve
    {
        public abstract Vector2 GetPoint(float t);
    }
}
