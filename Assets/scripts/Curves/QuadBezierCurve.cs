﻿using UnityEngine;

namespace Assets
{
    public class QuadBezierCurve : BezierCurve
    {
        private Vector2 _statringPoint;
        private Vector2 _endingPoint;
        private Vector2 _middlePoint;

        public QuadBezierCurve(Vector2 statringPoint, Vector2 middlePoint, Vector2 endingPoint)
        {
            _statringPoint = statringPoint;
            _middlePoint = middlePoint;
            _endingPoint = endingPoint;
        }

        public override Vector2 GetPoint(float t)
        {
            // More information can be found here: https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Quadratic_B%C3%A9zier_curves
            return (Mathf.Pow(1 - t, 2) * _statringPoint) + (2 * (1 - t) * t * _middlePoint) + (Mathf.Pow(t, 2) * _endingPoint);
        }
    }
}
