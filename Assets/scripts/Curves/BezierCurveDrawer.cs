﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class BezierCurveDrawer : IBezierCurveDrawer<BezierCurve>
    {
        private LineRenderer _lineRenderer;
        private int _segmentsAmount;
        // TODO: Because drawing is done here, layers should be regarded

        public BezierCurveDrawer(int segmentsAmount, LineRenderer lineRenderer)
        {
            _segmentsAmount = segmentsAmount;
            _lineRenderer = lineRenderer;
        }

        public void Draw(BezierCurve bezierCurve)
        {
            for (int i = 1; i <= _segmentsAmount; i++)
            {
                float t = i / (float) _segmentsAmount;
                _lineRenderer.positionCount++;
                _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, bezierCurve.GetPoint(t));
            }
        }

        public void Draw(IEnumerable<BezierCurve> bezierCurves)
        {
            foreach (BezierCurve curve in bezierCurves)
            {
                Draw(curve);
            }
        }
    }
}
