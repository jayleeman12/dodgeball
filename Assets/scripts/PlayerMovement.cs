﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    private const float _speedVertical = 1.0f;
    private const float _acceleration = 10.0f;
    private const float _disAcceleration = 20.0f;
    private const float _maximumSpeed = 10.0f;
    private const float _minimumSpeed = 0.0f;
    private const float _closeToZeroVelocity = 0.2f;

    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (Input.GetKey("right"))
        {
            if (rb2d.velocity.x < 0)
            {
                rb2d.velocity =
                    new Vector2(rb2d.velocity.x + Time.deltaTime * _disAcceleration, _speedVertical);
            }
            else if (rb2d.velocity.x < _maximumSpeed)
            {
                rb2d.velocity = 
                    new Vector2(rb2d.velocity.x + Time.deltaTime * _acceleration, _speedVertical);
            }
            else
            {
                rb2d.velocity =
                    new Vector2(_maximumSpeed, _speedVertical);
            }
        }
        else if (Input.GetKey("left"))
        {
            if (rb2d.velocity.x > 0)
            {
                rb2d.velocity =
                    new Vector2(rb2d.velocity.x - Time.deltaTime * _disAcceleration, _speedVertical);
            }
            else if (rb2d.velocity.x < _maximumSpeed)
            {
                rb2d.velocity =
                    new Vector2(rb2d.velocity.x - Time.deltaTime * _acceleration, _speedVertical);
            }
            else
            {
                rb2d.velocity =
                    new Vector2(-_maximumSpeed, _speedVertical);
            }
        }
        else
        {
            if (Math.Abs(rb2d.velocity.x) < _closeToZeroVelocity)
            {
                rb2d.velocity =
                    new Vector2(0, _speedVertical);
            }
            else if (rb2d.velocity.x > 0)
            {
                rb2d.velocity =
                    new Vector2(rb2d.velocity.x - Time.deltaTime * _disAcceleration, _speedVertical);
            }
            else if (rb2d.velocity.x < 0)
            {
                rb2d.velocity =
                    new Vector2(rb2d.velocity.x + Time.deltaTime * _disAcceleration, _speedVertical);
            }
        }
    }
}
