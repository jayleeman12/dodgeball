﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object

    private Vector2 offset;

    // Use this for initialization
    void Start () {
        offset = new Vector2(0, -0.25f);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.position = new Vector3(0, player.transform.position.y + offset.y, -10);
	}
}
