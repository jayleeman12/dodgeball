﻿using System.Collections.Generic;
using Assets;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class MapGenerator : MonoBehaviour
{
    public LineRenderer LineRenderer;

    private List<BezierCurve> _quadBezierCurves;
    private IBezierCurveDrawer<BezierCurve> _bezierCurveDrawer;
    public int SegmentsInCurve = 100;

    void Start()
    {
        _quadBezierCurves = new List<BezierCurve>
        {
            new QuadBezierCurve(new Vector2(0,0), new Vector2(1.5f,1.5f), new Vector2(0,3)),
            new QuadBezierCurve(new Vector2(0,3), new Vector2(-1.5f,4.5f), new Vector2(0,6))
        };
        _bezierCurveDrawer = new BezierCurveDrawer(SegmentsInCurve, LineRenderer);
        _bezierCurveDrawer.Draw(_quadBezierCurves);
    }

    private void DrawCurve()
    {
        for (int i = 0; i < _quadBezierCurves.Count; i++)
        {
            var currentCurve = _quadBezierCurves[i];
            for (int j = 1; j <= SegmentsInCurve; j++)
            {
                float t = j / (float) SegmentsInCurve;
                Vector2 pixel = currentCurve.GetPoint(t);
                int positionCount = (i * SegmentsInCurve) + j;
                LineRenderer.positionCount = positionCount;
                LineRenderer.SetPosition(positionCount - 1, pixel);
            }
        }
    }
}